<?php
/**
* Plugin Name: OrbitWeb Win Wheel plugin
* Plugin URI: https://orbitweb.ca/
* Description: OrbitWeb Win Wheel Plugin provide the posibility to create a wheel of fortune to won gifts, discounts and coupons.
* Version: 1.0
* Author: ADIS Ingenieros
* Author URI: http://adisingenieros.com/
**/
global $orbitDbVersion;
global $wpdb;
$optionTable = $wpdb->prefix . "oribit_options";
$optionRegistrationsTable = $wpdb->prefix . "oribit_registrations";
$optionUsageTable = $wpdb->prefix . "oribit_option_claimed";

require( ABSPATH.'wp-content/plugins/orbitwheel/includes/database-setup.php' );
require( ABSPATH.'wp-content/plugins/orbitwheel/includes/rest-api.php' );


function orbitwebWinWheelSettings() {
	add_option( 'orbitwebWinWheel-data_collection', true );
	add_option( 'orbitwebWinWheel-users_can_register', true );
	add_option( 'orbitwebWinWheel-max_spins', 3 );
	add_option( 'orbitwebWinWheel-form_position', 'bottom' );
	add_option( 'orbitwebWinWheel-users_phone_field', false );
}


function orbitwebWinWheelShortCode_US() {
	ob_start();
	//include(get_stylesheet_directory().'/orbitwheel/wheel.php');
	include(ABSPATH.'wp-content/plugins/orbitwheel/wheel_us.php');
	return ob_get_clean();
}
add_shortcode('orbitwebwinwheel_us', 'orbitwebWinWheelShortCode_US');

function orbitwebWinWheelShortCode_FR() {
	ob_start();
	//include(get_stylesheet_directory().'/orbitwheel/wheel.php');
	include(ABSPATH.'wp-content/plugins/orbitwheel/wheel_fr.php');
	return ob_get_clean();
}
add_shortcode('orbitwebwinwheel_fr', 'orbitwebWinWheelShortCode_FR');

function orbitweb_wheel_plugin() {
	add_menu_page( 'Orbitweb WinWheel', 'Orbitweb WinWheel', 'manage_options', 'orbitwebWinWheel', 'orbitweb_wheel_plugin_options' );

	if ( get_option( 'orbitwebWinWheel-data_collection' ) ) {
		add_submenu_page( 'orbitwebWinWheel', 'Orbitweb Winners', 'Orbitweb Winners', 'manage_options', 'winners', 'orbitweb_wheel_plugin_winners' );
	}
	
	add_submenu_page( 'orbitwebWinWheel', 'Orbitweb Settings', 'Orbitweb Settings', 'manage_options', 'settings', 'orbitweb_wheel_plugin_settings' );
}
add_action( 'admin_menu', 'orbitweb_wheel_plugin' );

function orbitweb_wheel_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include(ABSPATH.'wp-content/plugins/orbitwheel/wheel-options.php');
}

function orbitweb_wheel_plugin_winners() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include(ABSPATH.'wp-content/plugins/orbitwheel/wheel-winners.php');
}

function orbitweb_wheel_plugin_settings() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include(ABSPATH.'wp-content/plugins/orbitwheel/wheel-settings.php');
}

register_activation_hook( __FILE__, 'orbitwebWinWheelInstall' );
register_activation_hook( __FILE__, 'orbitwebWinWheelSettings' );
register_uninstall_hook(__FILE__, 'orbitwebWinWheelUninstall');
register_activation_hook( __FILE__, 'orbitwebWinWheelInstallData' );