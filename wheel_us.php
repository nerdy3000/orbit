<?php
    $maxSpins = get_option( 'orbitwebWinWheel-max_spins' );
    $dataCollection = get_option( 'orbitwebWinWheel-data_collection' );
    $usersCanRegister = get_option( 'orbitwebWinWheel-users_can_register' );
    $usersPhoneNumber = get_option( 'orbitwebWinWheel-users_phone_field' );
    $layout = get_option( 'orbitwebWinWheel-layout' );
    $wheelSegments = availableOptions();

    // Do not display form if data cannot be collected
    if ( !$dataCollection ) {
        $usersCanRegister = false;
    }
?>
<script type="text/javascript" src="/wp-content/plugins/orbitwheel/assets/lib/angular.min.js"></script>
<script src="/wp-content/plugins/orbitwheel/assets/lib/jquery.min.js"></script>
<link href="/wp-content/plugins/orbitwheel/assets/css/main.css" rel="stylesheet" type="text/css"></link>
<script type="text/javascript" src="/wp-content/plugins/orbitwheel/assets/js/Winwheel.min.js"></script>
<script type="text/javascript" src="/wp-content/plugins/orbitwheel/assets/js/orbittween.min.js"></script>
<div id="dv-congratulations">
    <label id="msg-win" class="animated rotateIn delay-5s">Congratulations, you've won! </label><label id="gift" class="animated infinite flash delay-5s"></label>
</div>
<div id="fireworks-container2">
     <img src="/wp-content/plugins/orbitwheel/assets/img/cancel.png"/>
    <canvas id="canvas-fireworks"></canvas>
</div>
<div id="wheel-container" class="pyro wheel-container" ng-app="app" ng-controller="app-controller">
    <div align="center" id="main-container">
        <div class="wheel-layout wheel-layout-<?php echo $layout; ?>">
            <div class="the_wheel" align="center" valign="center">
                <div id="wheel-arrow">
                    <img src="/wp-content/plugins/orbitwheel/assets/img/arrow.png"/>
                </div>
                <canvas id="canvas" width='800' height='800'>
                    <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please try another.</p>
                </canvas>
            </div>
            <div class="spin-form">
                <?php if ( $usersCanRegister ) { ?>
                    <div class="form-instructions">Please enter your name and email</div>
                    <div class="user-info">
                        <div class="form-input">
                            <input type="text" placeholder="NAME" id="user-name" class="user-name" value="" required />
                            <span id="user-name-error" class="error-message" style="display:none">Name is required</span>
                        </div>
                        <?php if ( $usersPhoneNumber ) { ?>
                            <div class="form-input">
                                <input type="tel" placeholder="PHONE" id="user-phone_number" class="user-phone_number" value="" />
                            </div>
                        <?php } ?>
                        <div class="form-input">
                            <input type="email" placeholder="EMAIL" id="user-email" class="user-email" value="" required />
                            <span id="user-email-error" class="error-message" style="display:none">Invalid email</span>
                        </div>
                        <div id="max-spin-indicator" class="max-spin-indicator" style="display:none">You have used <span id="used-spins"></span> of <span id="max-spins"></span> spins</div>
                    </div>
                <?php } ?>
                <button type="button" class="orbit-button" id="play-button">Spin to win</button>

                <?php if ( $usersCanRegister ) { ?>
                    <button type="button" class="reset-button" id="reset-button">Reset</button>
                <?php } ?>
            </div>
        </div>
        <section class="rain"></section>
    </div>
</div>
<script>
    
    angular.module("app", []).controller("app-controller", function($scope, $http) {

        var segments = <?php echo json_encode( $wheelSegments ); ?>;
        var number_of_segments = segments.length;
        let theWheel;
        
        var congrats = document.getElementById('dv-congratulations');
        var maxSpins = <?php echo $maxSpins; ?>;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var userName = document.getElementById( 'user-name' );
        var userEmail = document.getElementById( 'user-email' );
        var userPhoneNumber = document.getElementById( 'user-phone_number' );
        var userNameError = document.getElementById( 'user-name-error' );
        var userEmailError = document.getElementById( 'user-email-error' );
        var maxSpinIndicator = document.getElementById( 'max-spin-indicator' );
        var userUsedSpins = document.getElementById( 'used-spins' );
        var playButton = document.getElementById( 'play-button' );
        <?php if ( $usersCanRegister ) { ?>
            document.getElementById( 'max-spins' ).innerText = maxSpins;
        <?php } ?>
        var winwheelOptions = {
            'outerRadius': 380, // Set outer radius so wheel fits inside the background.
            'innerRadius': 200, // Make wheel hollow so segments don't go all way to center.
            'textFontSize': number_of_segments, // Set default font size for the segments.
            'lineWidth'   : 12,
            'textOrientation': 'vertical', // Make text vertial so goes down from the outside of wheel.
            'textAlignment': 'outer', // Align text to outside of wheel.
            'numSegments': number_of_segments, // Specify number of segments.
            'segments': segments, // Define segments including colour and text.
            'animation': // Specify the animation to use.
            {
                'type': 'spinToStop',
                'duration': 10, // Duration in seconds.
                'spins': 3, // Default number of complete spins.
                'callbackFinished': alertPrize,
                'callbackSound': playSound, // Function to call when the tick sound is to be triggered.
                'soundTrigger': 'pin' // Specify pins are to trigger the sound, the other option is 'segment'.
            },
            'pins': // Turn pins on.
            {
                'number': number_of_segments,
                'fillStyle': 'red',
                'outerRadius': 5,
                'margin': -20,
                'strokeStyle': 'red',
            }
        };

        theWheel = new Winwheel( winwheelOptions );
        
         // Loads the tick audio sound in to an audio object.
        let audio = new Audio('/wp-content/plugins/orbitwheel/assets/audio/tick.mp3');

        // This function is called when the sound is to be played.
        function playSound() {
            // Stop and rewind the sound if it already happens to be playing.
            audio.pause();
            audio.currentTime = 0;

            // Play the sound.
            audio.play();
        }

        // Vars used by the code in this page to do power controls.
        let wheelPower = 0;
        let wheelSpinning = false;
        //Defining high power default
        powerSelected(2);
        // -------------------------------------------------------
        // Function to handle the onClick on the power buttons.
        // -------------------------------------------------------
        function powerSelected(powerLevel) {
            // Ensure that power can't be changed while wheel is spinning.
            if ( wheelSpinning == false ) {
                // Set wheelPower var used when spin button is clicked.
                wheelPower = powerLevel;
            }
        }

        // -------------------------------------------------------
        // Click handler for spin button.
        // -------------------------------------------------------
        function startSpin() {
            // Ensure that spinning can't be clicked again while already running.
            if (wheelSpinning == false) {
                // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                // to rotate with the duration of the animation the quicker the wheel spins.
                if (wheelPower == 1) {
                    theWheel.animation.spins = 3;
                } else if (wheelPower == 2) {
                    theWheel.animation.spins = 6;
                } else if (wheelPower == 3) {
                    theWheel.animation.spins = 10;
                }

                // Begin the spin animation by calling startAnimation on the wheel object.
                theWheel.startAnimation();
                $("#wheel-arrow img").addClass( 'shake' );
                // Set to true so that power can't be changed and spin button re-enabled during
                // the current animation. The user will have to reset before spinning again.
                wheelSpinning = true;
            }
        }

        // -------------------------------------------------------
        // Function for reset button.
        // -------------------------------------------------------
        function resetWheel() {
            var playButton = document.getElementById('play-button');
            playButton.style.visibility = 'visible';
            playButton.innerHTML = 'Spin Now';
            
            theWheel.stopAnimation(false); // Stop the animation, false as param so does not call callback function.
            theWheel.rotationAngle = 0; // Re-set the wheel angle to 0 degrees.
            theWheel.draw(); // Call draw to render changes to the wheel.

            document.getElementById("fireworks-container2").style.display = 'none';
            wheelSpinning = false; // Reset to false to power buttons and spin can be clicked again.
            congrats.style.visibility = 'hidden';
        }

        $("#fireworks-container2 img").on("click", function(e){
            resetWheel();
        });

        $("#fireworks-container img").on("click", function(e){
            resetWheel();
        });
        // -------------------------------------------------------
        // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
        // -------------------------------------------------------
        function startAgain(playButton){
            //playButton.innerHTML = 'Tourne encore?';
            //playButton.setAttribute('onClick', 'resetWheel();');
                playButton.style.visibility = 'hidden';
                setTimeout(function(){
                resetWheel();
                },5000);//5000 millisecond
        }

        function alertPrize(indicatedSegment) {
            // Just alert to the user what happened.
            // In a real project probably want to do something more interesting than this with the result.
            $("#wheel-arrow img").removeClass( 'shake' );
            var gift = document.getElementById('gift');
            congrats.style.visibility =  'visible';
            gift.innerHTML = indicatedSegment.text;
            $("#dv-congratulations label").animate({fontSize: '45px'});
            let audio = new Audio("/wp-content/plugins/orbitwheel/assets/audio/wingamesound.wav");
            audio.play();
            document.getElementById("fireworks-container2").style.display = 'block';
            var playButton = document.getElementById('play-button');

            <?php if ( $dataCollection ) { ?>
                var data = {
                    'optionId': indicatedSegment.id,
                }

                <?php if ( $usersCanRegister ) { ?>
                    data.email = userEmail.value;
                <?php } ?>
                $http({
                    url: '/wp-json/orbit/v1/register_spin/',
                    method: "POST",
                    data: JSON.stringify(data)
                })
                .then(function(response) {
                    <?php if ( $usersCanRegister ) { ?>
                        if ( response.data.usedSpins ) {
                            var spinsUsed = parseInt( response.data.usedSpins, 10 );

                            maxSpinIndicator.style.display = 'block';
                            userUsedSpins.innerText = spinsUsed;

                            if ( spinsUsed >= maxSpins ) {
                                $( maxSpinIndicator ).addClass( 'no-spins-left' );
                                playButton.disabled = true;
                            }
                        }
                    <?php } ?>
                    startAgain( playButton ); 
                });
            <?php } else { ?>
                startAgain( playButton ); 
            <?php } ?>
        }

        resetWheel();

        function submitUserInformation() {

            // Do not trigger if wheel is currently spinning
            if ( wheelSpinning ) {
                return;
            }

            
            <?php if ( $usersCanRegister ) { ?>
                var emailIsValid = emailRegex.test( String( userEmail.value ).toLowerCase() );
                var formIsValid = true;

                if ( userName.value && userName.value.length ) {
                    userNameError.style.display = 'none';
                } else {
                    formIsValid = false;
                    userNameError.style.display = 'block';
                }

                if ( userEmail.value && userEmail.value.length && emailIsValid ) {
                    userEmailError.style.display = 'none';
                } else {
                    formIsValid = false; 
                    userEmailError.style.display = 'block';
                }

                if ( formIsValid ) {
                    var data = {
                        'name': userName.value,
                        'email': userEmail.value,
                        'language': 'us',
                    };

                    <?php if ( $usersPhoneNumber ) { ?>
                        data.phone_number = userPhoneNumber.value;
                    <?php } ?>
                    $http({
                        url: '/wp-json/orbit/v1/register_user/',
                        method: "POST",
                        data: JSON.stringify(data)
                    })
                    .then(function(response) {
                        if ( response.data.usedSpins ) {
                            var spinsUsed = parseInt( response.data.usedSpins, 10 );
                            spinsUsed++;

                            maxSpinIndicator.style.display = 'block';

                            if ( spinsUsed < maxSpins ) {
                                userUsedSpins.innerText = spinsUsed;
                            } else {
                                userUsedSpins.innerText = maxSpins;
                            }

                            if ( spinsUsed <= maxSpins ) {
                                $( maxSpinIndicator ).removeClass( 'no-spins-left' );
                                startSpin();
                            } else {
                                $( maxSpinIndicator ).addClass( 'no-spins-left' );
                            }
                        }
                    });
                }
            <?php } else { ?>
                startSpin();
            <?php } ?>
        }

        playButton.onclick = submitUserInformation;

        <?php if ( $usersCanRegister ) { ?>
            document.getElementById( 'reset-button' ).onclick = function() {
                userName.value = '';
                userEmail.value = '';

                <?php if ( $usersPhoneNumber ) { ?>
                    userPhoneNumber.value = '';
                <?php } ?>
                maxSpinIndicator.style.display = 'none';
                userNameError.style.display = 'none';
                userEmailError.style.display = 'none';
                resetWheel();
            };

            function renablePlay() {
                if ( playButton.disabled ) {
                    playButton.disabled = false;

                    maxSpinIndicator.style.display = 'none';
                    maxSpinIndicator.removeClass( 'no-spins-left' );
                }
            }

            userEmail.onpaste = renablePlay;
            userEmail.onkeyup = renablePlay;
        <?php } ?>

    });
 
</script>
<script>
window.addEventListener("resize", resizeCanvas, false);
        window.addEventListener("DOMContentLoaded", onLoad, false);
        
        window.requestAnimationFrame = 
            window.requestAnimationFrame       || 
            window.webkitRequestAnimationFrame || 
            window.mozRequestAnimationFrame    || 
            window.oRequestAnimationFrame      || 
            window.msRequestAnimationFrame     || 
            function (callback) {
                window.setTimeout(callback, 1000/60);
            };
        
        var canvas, ctx, w, h, particles = [], probability = 0.04,
            xPoint, yPoint;
        
        
        
        
        
        function onLoad() {
            canvas = document.getElementById("canvas-fireworks");
            ctx = canvas.getContext("2d");
            resizeCanvas();
            
            window.requestAnimationFrame(updateWorld);
        } 
        
        function resizeCanvas() {
            if (!!canvas) {
                w = canvas.width = screen.width-100;
                h = canvas.height = '950';
            }
        } 
        
        function updateWorld() {
            update();
            paint();
            window.requestAnimationFrame(updateWorld);
        } 
        
        function update() {
            if (particles.length < 500 && Math.random() < probability) {
                createFirework();
            }
            var alive = [];
            for (var i=0; i<particles.length; i++) {
                if (particles[i].move()) {
                    alive.push(particles[i]);
                }
            }
            particles = alive;
        } 
        
        function paint() {
            ctx.globalCompositeOperation = 'source-over';
            ctx.fillStyle = "rgba(0,0,0,0.4)";
            ctx.fillRect(0, 0, w, h);
            ctx.globalCompositeOperation = 'lighter';
            for (var i=0; i<particles.length; i++) {
                particles[i].draw(ctx);
            }
            ctx.globalAlpha = '0.5';
        } 
        
        function createFirework() {
            xPoint = Math.random()*(w-200)+100;
            yPoint = Math.random()*(h-200)+100;
            var nFire = Math.random()*50+100;
            var c = "rgb("+(~~(Math.random()*200+55))+","
                 +(~~(Math.random()*200+55))+","+(~~(Math.random()*200+55))+")";
            for (var i=0; i<nFire; i++) {
                var particle = new Particle();
                particle.color = c;
                var vy = Math.sqrt(25-particle.vx*particle.vx);
                if (Math.abs(particle.vy) > vy) {
                    particle.vy = particle.vy>0 ? vy: -vy;
                }
                particles.push(particle);
            }
        } 
        
        function Particle() {
            this.w = this.h = Math.random()*4+1;
            
            this.x = xPoint-this.w/2;
            this.y = yPoint-this.h/2;
            
            this.vx = (Math.random()-0.5)*10;
            this.vy = (Math.random()-0.5)*10;
            
            this.alpha = Math.random()*.5+.5;
            
            this.color;
        } 
        
        Particle.prototype = {
            gravity: 0.05,
            move: function () {
                this.x += this.vx;
                this.vy += this.gravity;
                this.y += this.vy;
                this.alpha -= 0.01;
                if (this.x <= -this.w || this.x >= screen.width ||
                    this.y >= screen.height ||
                    this.alpha <= 0) {
                        return false;
                }
                return true;
            },
            draw: function (c) {
                c.save();
                c.beginPath();
                
                c.translate(this.x+this.w/2, this.y+this.h/2);
                c.arc(0, 0, this.w, 0, Math.PI*2);
                c.fillStyle = this.color;
                c.globalAlpha = this.alpha;
                
                c.closePath();
                c.fill();
                c.restore();
            }
        } 
</script>
