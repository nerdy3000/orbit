<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div ng-app="app" ng-controller="app-controller">
	<div><h2>OrbitWeb Wheel configuration page</h2></div>
	<br/><br/>
<form action="#">
  <div>
  Number of wheel options:<br/>
  <input type="number" name="number_of_options" id="number_of_options">
</div>
  <br/>
  <div>
  <button type="button" id="btn-save">Save</button> 
</div>
  <br/>
  <div id="options_container">
  	<h4>Set configuration</h4>
  </div>
  <br/> 
  <div id="success-info"><label style="color:#3CB371">Configuration saved successfully!</label></div>
   <br/>
  <button type="button" id="btn-save-config">Save configurations</button>  
<br/>
  
</form> 
</div>
<style type="text/css">
	#success-info{
		visibility: hidden;
	}
	#options_container{
		visibility: hidden;
	}
	#btn-save-config{
		visibility: hidden;
	}
</style>
<script>
//document.addEventListener("DOMContentLoaded", function(event) {
angular.module("app", []).controller("app-controller", function($scope, $http) {
    var btn = document.getElementById('btn-save');
    var btnSaveConfig = document.getElementById('btn-save-config');
    var number_of_options = document.getElementById('number_of_options');
    var node_container_options = document.getElementById('options_container');
    var message = document.getElementById('success-info');
    var data = [];
    var loadedOptions = [];

	function removeElementsByClass(className){
	    var elements = document.getElementsByClassName(className);
	    while(elements.length > 0){
	        elements[0].parentNode.removeChild(elements[0]);
	    }
	}

    function setOptions(number_of_options, options) {
    	node_container_options.innerHTML = '';
    	node_container_options.style.visibility = 'visible';
        btnSaveConfig.style.visibility = 'visible';
        if (options != null) {
            for (var i = 0; i < number_of_options; i++) {
                var divider1 = document.createElement('div');
                var optionId = document.createElement('input');
                var optionColor = document.createElement('input');
                var optionText = document.createElement('input');
                var optionFontSize = document.createElement('input');
                var optionTextColor = document.createElement('input');
                var optionMaxUses = document.createElement('input');
                var optionPercent = document.createElement('input');
                var optionStatus = document.createElement('input');

                divider1.className = 'divider';
                var br = document.createElement('br');
                //Definning option color attributes
                optionId.name = 'option-id-' + i;
                optionId.type = 'hidden';
                optionId.className = 'option-id';
                optionId.value = options[i].id;

                optionColor.name = 'option-color-' + i;
                optionColor.placeholder = 'Color (Hexadecimal)';
                optionColor.type = 'text';
                optionColor.className = 'option-color';
                optionColor.value = options[i].fillStyle;

                optionText.name = 'option-text-' + i;
                optionText.placeholder = 'Text';
                optionText.type = 'text';
                optionText.className = 'option-text';
                optionText.value = options[i].text;

                optionFontSize.name = 'option-font-' + i;
                optionFontSize.placeholder = 'Font Size (number)';
                optionFontSize.type = 'text';
                optionFontSize.className = 'option-font';
                optionFontSize.value = options[i].textFontSize;

                optionTextColor.name = 'option-textcolor-' + i;
                optionTextColor.placeholder = 'Text Color (Hexadecimal)';
                optionTextColor.type = 'text';
                optionTextColor.className = 'option-textcolor';
                optionTextColor.value = options[i].textFillStyle;

                optionMaxUses.name = 'option-maxuses-' + i;
                optionMaxUses.placeholder = 'Max. Wins';
                optionMaxUses.type = 'text';
                optionMaxUses.className = 'option-maxuses';
                optionMaxUses.value = options[i].maxUses;

                optionPercent.name = 'option-percent-' + i;
                optionPercent.placeholder = 'Percent';
                optionPercent.type = 'text';
                optionPercent.className = 'option-percent';
                optionPercent.value = options[i].percent;

                optionStatus.name = 'option-status-' + i;
                optionStatus.type = 'checkbox';
                optionStatus.className = 'option-status';
                optionStatus.value = 1;
                if ( options[i].status !== "0" ) {
                    optionStatus.checked = true;
                }

                divider1.appendChild(optionStatus);
                divider1.appendChild(optionId);
                divider1.appendChild(optionColor);
                divider1.appendChild(optionText);
                divider1.appendChild(optionFontSize);
                divider1.appendChild(optionTextColor);
                divider1.appendChild(optionMaxUses);
                divider1.appendChild(optionPercent);
                divider1.appendChild(optionPercent);
                node_container_options.appendChild(divider1);

                node_container_options.appendChild(br);
            }
        } else {
            for (var i = 0; i < number_of_options.value; i++) {
                var divider1 = document.createElement('div');


                var optionId = document.createElement('input');
                var optionColor = document.createElement('input');
                var optionText = document.createElement('input');
                var optionFontSize = document.createElement('input');
                var optionTextColor = document.createElement('input');
                var optionMaxUses = document.createElement('input');
                var optionPercent = document.createElement('input');
                var optionStatus = document.createElement('input');
                var br = document.createElement('br');

                //Definning option color attributes
                optionId.name = 'option-id-' + i;
                optionId.type = 'hidden';
                optionId.className = 'option-id';

                optionColor.name = 'option-color-' + i;
                optionColor.placeholder = 'Color Hexadecimal';
                optionColor.type = 'text';
                optionColor.className = 'option-color';

                optionText.name = 'option-text-' + i;
                optionText.placeholder = 'Text';
                optionText.type = 'text';
                optionText.className = 'option-text';

                optionFontSize.name = 'option-font-' + i;
                optionFontSize.placeholder = 'Font Size';
                optionFontSize.type = 'text';
                optionFontSize.className = 'option-font';

                optionTextColor.name = 'option-textcolor-' + i;
                optionTextColor.placeholder = 'Text Color Hexadecimal';
                optionTextColor.type = 'text';
                optionTextColor.className = 'option-textcolor';

                optionMaxUses.name = 'option-maxuses-' + i;
                optionMaxUses.placeholder = 'Max. Wins';
                optionMaxUses.type = 'text';
                optionMaxUses.className = 'option-maxuses';

                optionPercent.name = 'option-percent-' + i;
                optionPercent.placeholder = 'Text Color Hexadecimal';
                optionPercent.type = 'text';
                optionPercent.className = 'option-percent';

                optionStatus.name = 'option-status-' + i;
                optionStatus.type = 'checkbox';
                optionStatus.className = 'option-status';
                optionStatus.value = 1;
                optionStatus.checked = true;

                divider1.appendChild(optionId);
                divider1.appendChild(optionColor);
                divider1.appendChild(optionText);
                divider1.appendChild(optionFontSize);
                divider1.appendChild(optionTextColor);
                divider1.appendChild(optionMaxUses);
                divider1.appendChild(optionPercent);
                divider1.appendChild(optionStatus);
                node_container_options.appendChild(divider1);

                node_container_options.appendChild(br);
            }
        }

    }

    $http({
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        url: '/wp-json/orbit/v1/options/',
    }).then(function(response) {
        var wellData = response.data;
        if (wellData.length > 0) {
        	number_of_options.value = wellData.length;
            loadedOptions = wellData;
            setOptions( number_of_options.value, wellData );
        }
    }, function(response) {
        console.log(response);
    });

    btn.onclick = function(e) {
        //node_container_options.style.visibility = 'visible';
        var sendData = [];
        for( var i = 0; i < number_of_options.value; i++ ) {
            if ( loadedOptions.length > i ) {
                sendData.push( loadedOptions[i] );
            } else {
                sendData.push( {
                    "id": "",
                    "fillStyle": "",
                    "text": "",
                    "textFontSize": "",
                    "textFillStyle": "",
                    "strokeStyle": "#FFF",
                    "percent": null,
                    "maxUses": null,
                    "used": "0",
                    "status": "1",
                });
            }
        }
        setOptions( number_of_options.value, sendData );
        //btnSaveConfig.style.visibility = 'visible';

    }

    btnSaveConfig.onclick = function(e) {
        var optionIdArray = [];
        var optionTextArray = [];
        var optionColorArray = [];
        var optionFontArray = [];
        var optionTextColorArray = [];
        var optionMaxUsesArray = [];
        var optionPercentArray = [];
        var optionsArray = [];
        var optionStatusArray = [];
        data = {};
        $('#options_container input').each(function(e) {
            if (this.className == 'option-id') {
                optionIdArray.push({
                    id: this.value
                });
            } else if (this.className == 'option-text') {
                optionTextArray.push({
                    text: this.value
                });
            } else if (this.className == 'option-textcolor') {
                optionTextColorArray.push({
                    textFillStyle: this.value
                });
            } else if (this.className == 'option-font') {
                optionFontArray.push({
                    textFontSize: this.value
                });
            } else if (this.className == 'option-color') {
                optionColorArray.push({
                    fillStyle: this.value
                });
            } else if (this.className == 'option-maxuses') {
                optionMaxUsesArray.push({
                    maxUses: this.value
                });
            } else if (this.className == 'option-percent') {
                optionPercentArray.push({
                    percent: this.value
                });
            } else if (this.className == 'option-status') {
                optionStatusArray.push({
                    status: ( this.checked )? 1 : 0,
                });
            }
        });

        for (var i = 0; i < optionTextArray.length; i++) {
            optionsArray.push({
                id: optionIdArray[i].id,
                fillStyle: optionColorArray[i].fillStyle,
                text: optionTextArray[i].text,
                textFontSize: optionFontArray[i].textFontSize,
                textFillStyle: optionTextColorArray[i].textFillStyle,
                strokeStyle: '#FFF',
                maxUses: optionMaxUsesArray[i].maxUses,
                percent: optionPercentArray[i].percent,
                status: optionStatusArray[i].status,
            });
        }
        data.options = optionsArray;

        $http({
                url: '/wp-json/orbit/v1/set_options/',
                method: "POST",
                data: JSON.stringify( data )
            })
            .then(function(response) {
                var wellData = response.data;
                if ( wellData.length > 0 ) {
                    number_of_options.value = wellData.length;
                    loadedOptions = wellData;
                    setOptions( number_of_options.value, wellData );
                }
                message.style.visibility = 'visible';
                
            },
            function(response) { // optional
                //failed
                console.log(response);
            });
    }

});
</script>