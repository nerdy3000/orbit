<?php
$usersPhoneNumber = get_option( 'orbitwebWinWheel-users_phone_field' );
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<div ng-app="app" ng-controller="app-controller">
    <div class="wrap">
        <h1 class="wp-heading-inline">OrbitWeb Wheel Winners</h1>
        <button type="button" id="btn-download" class="components-button is-primary">Download</button>
        <div id="winners_container"></div>
    </div>
</div>
<style type="text/css">
	#success-info{
		visibility: hidden;
	}
	#winners_container{
		visibility: hidden;
	}
	#btn-download{
		visibility: hidden;
    }

    .winners-table {
        margin-right: 20px;
    }

    .prize .prize-details {
        display: flex;
    }
    .prize .prize-details .colour-indicator {
        width: 15px;
        height: 15px;
        border: 1px solid #ddd;
        margin-right: 10px;
    }

    .prize .prize-time {
        margin-left: 25px;
        font-size: 0.8em;
        color: #777;
    }
    
    .prize + .prize {
        border-top: 1px solid #ddd;
        padding-top: 5px;
    }
</style>
<script>
//document.addEventListener("DOMContentLoaded", function(event) {
angular.module("app", []).controller("app-controller", function($scope, $http) {
    var node_container_winners = document.getElementById('winners_container');
    var download_button = document.getElementById('btn-download');
    var message = document.getElementById('success-info');
    var data = [];
    var showPhone = <?php if ( $usersPhoneNumber ) { echo 'true'; } else { echo 'false'; } ?>;

function setWinners( winners ) {
    
    if ( winners != null && winners.length > 0 ) {
        var winnersTable = document.createElement( 'table' );
        winnersTable.className = 'wp-list-table widefat fixed striped pages winners-table';

        var winnersThead = document.createElement( 'thead' );
        var winnersTbody = document.createElement( 'tbody' );
        var winnersTheadRow = document.createElement( 'tr' );

        var winnersTheadCellName = document.createElement( 'th' );
        winnersTheadCellName.className = 'manage-column';
        winnersTheadCellName.innerText = 'Name';
        winnersTheadRow.appendChild( winnersTheadCellName );

        var winnersTheadCellEmail = document.createElement( 'th' );
        winnersTheadCellEmail.className = 'manage-column';
        winnersTheadCellEmail.innerText = 'Email';
        winnersTheadRow.appendChild( winnersTheadCellEmail );

        if ( showPhone ) {
            var winnersTheadCellPhone = document.createElement( 'th' );
            winnersTheadCellPhone.className = 'manage-column';
            winnersTheadCellPhone.innerText = 'Phone';
            winnersTheadRow.appendChild( winnersTheadCellPhone );
        }

        var winnersTheadCellLanguage = document.createElement( 'th' );
        winnersTheadCellLanguage.className = 'manage-column';
        winnersTheadCellLanguage.innerText = 'Language';
        winnersTheadRow.appendChild( winnersTheadCellLanguage );

        var winnersTheadCellDate = document.createElement( 'th' );
        winnersTheadCellDate.className = 'manage-column';
        winnersTheadCellDate.innerText = 'Registered Date';
        winnersTheadRow.appendChild( winnersTheadCellDate );

        var winnersTheadCellPrizes = document.createElement( 'th' );
        winnersTheadCellPrizes.className = 'manage-column';
        winnersTheadCellPrizes.innerText = 'Prizes';
        winnersTheadRow.appendChild( winnersTheadCellPrizes );
        
        winnersThead.appendChild( winnersTheadRow );
        winnersTable.appendChild( winnersThead );

        for (var i = 0; i < winners.length; i++) {
            var divider1 = document.createElement('tr');
            var userName = document.createElement('td');
            var userEmail = document.createElement('td');
            var userPhone = document.createElement('td');
            var userLanguage = document.createElement('td');
            var createdAt = document.createElement('td');
            var prizes = document.createElement('td');

            divider1.className = 'divider user';

            userName.className = 'user-name';
            userName.innerText = winners[i].name;

            userEmail.className = 'user-email';
            userEmail.innerText = winners[i].email;

            userPhone.className = 'user-phone_number';
            userPhone.innerText = winners[i].phone_number;

            userLanguage.className = 'user-language';
            userLanguage.innerText = winners[i].language;

            createdAt.className = 'user-createdAt';
            createdAt.innerText = moment( winners[i].createdAt ).format( "MMM DD YYYY h:mma" );

            prizes.className = 'user-prizes';
            
            for ( var j = 0; j < winners[i].wins.length; j++) {
                var prize = winners[i].wins[j];
                var prizeWrapper = document.createElement('div');
                var prizeDetails = document.createElement('div');
                var prizeColour = document.createElement('div');
                var prizeText = document.createElement('div');
                var prizeTime = document.createElement('div');

                prizeColour.className = 'colour-indicator';
                prizeColour.style.background = prize.fillStyle;
                prizeColour.style.borderColor = prize.strokeStyle;
                prizeText.innerText = ( prize.text )? prize.text : 'N/A';
                prizeDetails.className = 'prize-details';
                prizeDetails.appendChild( prizeColour );
                prizeDetails.appendChild( prizeText );

                prizeTime.className = 'prize-time';
                prizeTime.innerText = moment( prize.wonAt ).format( "MMM DD YYYY h:mma" );

                prizeWrapper.className = 'prize';
                prizeWrapper.appendChild( prizeDetails );
                prizeWrapper.appendChild( prizeTime );
                prizes.appendChild( prizeWrapper );
            }

            divider1.appendChild( userName );
            divider1.appendChild( userEmail );

            if ( showPhone ) {
                divider1.appendChild( userPhone );
            }

            divider1.appendChild( userLanguage );
            divider1.appendChild( createdAt );
            divider1.appendChild( prizes );
            winnersTbody.appendChild( divider1 );
        }
        winnersTable.appendChild( winnersTbody );
        node_container_winners.appendChild( winnersTable );
    } else {
        var divider1 = document.createElement('div');
        var emptyMessage = document.createElement('div');

        emptyMessage.className = "empty-response";
        emptyMessage.innerText = "There are no registered winners";

        divider1.appendChild( emptyMessage );
        node_container_winners.appendChild( divider1 );
    }
}

function setPrizes( prizes ) {

    if ( prizes != null && prizes.length > 0 ) {
        var prizesTable = document.createElement( 'table' );
        prizesTable.className = 'wp-list-table widefat fixed striped pages prizes-table';

        var prizesThead = document.createElement( 'thead' );
        var prizesTbody = document.createElement( 'tbody' );
        var prizesTheadRow = document.createElement( 'tr' );

        var prizesTheadCellDetails = document.createElement( 'th' );
        prizesTheadCellDetails.className = 'manage-column';
        prizesTheadCellDetails.innerText = 'Details';
        prizesTheadRow.appendChild( prizesTheadCellDetails );

        var prizesTheadCellDate = document.createElement( 'th' );
        prizesTheadCellDate.className = 'manage-column';
        prizesTheadCellDate.innerText = 'Won at';
        prizesTheadRow.appendChild( prizesTheadCellDate );
        
        prizesThead.appendChild( prizesTheadRow );
        prizesTable.appendChild( prizesThead );
        

        for (var i = 0; i < prizes.length; i++) {
            var prize = prizes[i];
            var prizeWrapper = document.createElement('tr');
            var prizeDetails = document.createElement('td');
            var prizeTime = document.createElement('td');
            var prizeColour = document.createElement('div');
            var prizeText = document.createElement('div');

            prizeColour.className = 'colour-indicator';
            prizeColour.style.background = prize.fillStyle;
            prizeColour.style.borderColor = prize.strokeStyle;
            prizeText.innerText = ( prize.text )? prize.text : 'N/A';
            prizeDetails.className = 'prize-details';
            prizeDetails.appendChild( prizeColour );
            prizeDetails.appendChild( prizeText );

            prizeTime.className = 'prize-time';
            prizeTime.innerText = moment( prize.wonAt ).format( "MMM DD YYYY h:mma" );

            prizeWrapper.className = 'prize';
            prizeWrapper.appendChild( prizeDetails );
            prizeWrapper.appendChild( prizeTime );
            prizesTbody.appendChild( prizeWrapper );
        }
        
        prizesTable.appendChild( prizesTbody );
        node_container_winners.appendChild( prizesTable );
    }
}

function download() {
    $http({
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        url: '/wp-json/orbit/v1/winners_download/',
    }).then( function( response ) {
        if ( response.data.file ) {
            var anchor = angular.element('<a/>');
            anchor.css({display: 'none'}); // Make sure it's not visible
            angular.element(document.body).append(anchor); // Attach to document

            anchor.attr({
                href: response.data.file,
                target: '_blank',
                download: 'winners.csv'
            })[0].click();
            anchor.remove(); // Clean it up afterwards
        }
    });
}

    $http({
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        url: '/wp-json/orbit/v1/winners/',
    }).then(function(response) {
        var wellData = response.data;
        node_container_winners.innerHTML = '';
        node_container_winners.style.visibility = 'visible';
        download_button.style.visibility = 'visible';
        download_button.onclick = function() {
            download();
        };

        setWinners( wellData.winners );
        setPrizes( wellData.prizes );
    }, function(response) {
        console.log(response);
    });

});
</script>