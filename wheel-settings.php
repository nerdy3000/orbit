<?php

$maxSpins = get_option( 'orbitwebWinWheel-max_spins' );
$usersCanRegister = get_option( 'orbitwebWinWheel-users_can_register' );
$usersPhoneNumber = get_option( 'orbitwebWinWheel-users_phone_field' );
$layout = get_option( 'orbitwebWinWheel-layout' );
$layoutOptions = array( 'vertical', 'horizontal' );
$dataCollection = get_option( 'orbitwebWinWheel-data_collection' );
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js">
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div ng-app="app" ng-controller="app-controller">
    <div class="wrap">
	    <h1 class="wp-heading-inline">OrbitWeb Wheel Settings</h1>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">Collect Data</th>
                    <td>
                        <fieldset>
                            <label for="data_collection">
                                <input name="data_collection" type="checkbox" id="data_collection" value="1" <?php if ( $dataCollection ){ echo 'checked="true"';} ?>>
                                Collect Prize Data
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Registration</th>
                    <td>
                        <fieldset>
                            <label for="users_can_register">
                                <input name="users_can_register" type="checkbox" id="users_can_register" value="1" <?php if ( $usersCanRegister ){ echo 'checked="true"';} ?>>
                                Users must register to spin
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Enable Phone Number</th>
                    <td>
                        <fieldset>
                            <label for="users_phone_field">
                                <input name="users_phone_field" type="checkbox" id="users_phone_field" value="1" <?php if ( $usersPhoneNumber ){ echo 'checked="true"';} ?>>
                                Users may provide a phone number
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="max_spins">Max Spins per Email ( 0 for unlimited )</label></th>
                    <td><input name="max_spins" type="number" step="1" id="max_spins" value="<?php echo $maxSpins; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="layout">Layout</label></th>
                    <td>
                        <select name="layout" id="layout">
                            <?php foreach ( $layoutOptions as $option ) { ?>
                                <option value="<?php echo $option; ?>"
                                    <?php if ( $layout === $option ) { echo ' selected="selected"'; } ?>
                                ><?php echo $option; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><button type="button" name="submit" id="submitSettings" class="button button-primary">Save Changes</button></p>
        <div id="success-info"><label style="color:#3CB371">Configuration saved successfully!</label></div>
    </div>
</div>
<style type="text/css">
	#success-info{
		visibility: hidden;
	}
</style>
<script>
//document.addEventListener("DOMContentLoaded", function(event) {
angular.module("app", []).controller("app-controller", function($scope, $http) {
    var message = document.getElementById('success-info');
    var dataCollection = document.getElementById('data_collection');
    var maxSpins = document.getElementById('max_spins');
    var usersCanRegister = document.getElementById('users_can_register');
    var usersPhoneNumber = document.getElementById('users_phone_field');
    var layoutSelect = document.getElementById('layout');

    function updateSettings() {

        var data = {
            'max_spins': parseInt( maxSpins.value, 10 ),
            'data_collection': dataCollection.checked,
            'users_can_register': usersCanRegister.checked,
            'users_phone_field': usersPhoneNumber.checked,
            'layout': layoutSelect.options[ layoutSelect.selectedIndex ].value,
        };

        $http({
            url: '/wp-json/orbit/v1/settings/',
            method: "POST",
            data: JSON.stringify( data )
        })
        .then(function(response) {
            message.style.visibility = 'visible';
        },
        function(response) { // optional
            //failed
            console.log(response);
        });
    }

    document.getElementById('submitSettings').onclick = updateSettings;
});
</script>