<?php
function orbitwebWinWheelInstall() {
    include( 'database-variables.php' );
	
	$sql = "CREATE TABLE $optionTable (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		fillStyle tinytext,
		text text,
		textFontSize tinytext,
		textFillStyle tinytext,
		strokeStyle tinytext,
		image_url tinytext,
		percent int(100),
		maxUses int,
		status int(1) NOT NULL DEFAULT 1,
		createdAt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		PRIMARY KEY (id)
	);";
	$wpdb->query( $sql );

	$sql = "CREATE TABLE $optionRegistrationsTable (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		name tinytext NOT NULL,
		email tinytext NOT NULL,
		phone_number tinytext NOT NULL,
		language varchar(2) NOT NULL,
		status int(1) NOT NULL DEFAULT 1,
		createdAt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		PRIMARY KEY  (id)
	);";
	$wpdb->query( $sql );

	$sql = "CREATE TABLE $optionUsageTable (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		userId int DEFAULT NULL,
		optionId int NOT NULL,
		createdAt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		PRIMARY KEY  (id)
	);";
	$wpdb->query( $sql );

	add_option( 'orbit_db_version', $orbitDbVersion );
}

function orbitwebWinWheelUninstall() {
    include( 'database-variables.php' );

	$sql = "DROP TABLE IF EXISTS {$optionTable}";
	$wpdb->query( $sql );

	$sql = "DROP TABLE IF EXISTS {$optionRegistrationsTable}";
	$wpdb->query( $sql );

	$sql = "DROP TABLE IF EXISTS {$optionUsageTable}";
	$wpdb->query( $sql );
}

function orbitwebWinWheelInstallData() {
    include( 'database-variables.php' );
    
	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#E12490", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#E12490",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#00A997", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#00A997",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#F26829", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#F26829",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#0E4C90", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#0E4C90",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#F8AB3F", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#F8AB3F",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#E12490", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#E12490",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#00A997", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#00A997",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#F26829", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#F26829",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#0E4C90", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#0E4C90",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);

	$wpdb->insert( 
		$optionTable, 
		array( 
			'fillStyle' => "#F8AB3F", 
			'text' => "", 
			'textFontSize' => "", 
			'textFillStyle' => "", 
			'strokeStyle' => "#F8AB3F",
			'createdAt' => current_time( 'mysql' ), 
		) 
	);
}