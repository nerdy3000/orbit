<?php

function availableOptions() {
    include( 'database-variables.php' );

	$sql = "SELECT id, fillStyle, text, textFontSize, textFillStyle, strokeStyle, percent, maxUses, ( SELECT COUNT( id ) FROM {$optionUsageTable} WHERE {$optionUsageTable}.optionId = {$optionTable}.id ) as 'used' FROM {$optionTable} WHERE status = 1 ORDER BY id ASC";
	$options = $wpdb->get_results( $sql, OBJECT );

	$response = array();
	foreach( $options as $opt ) {
		if ( !$opt->maxUses || $opt->used < $opt->maxUses ) {
			$response[] = $opt;
		}
    }
    return $response;
}

function getAvailableOptionsApi() {
    return rest_ensure_response( availableOptions() );
}

function getAllOptionsApi() {
    include( 'database-variables.php' );

	$sql = "SELECT {$optionTable}.* FROM {$optionTable} ORDER BY id ASC";
	$options = $wpdb->get_results( $sql, OBJECT );
 
    return rest_ensure_response( $options );
}

function getAllOptionsWinnersApi() {
    include( 'database-variables.php' );

	$sql = "SELECT {$optionTable}.*, {$optionRegistrationsTable}.name, {$optionRegistrationsTable}.email, {$optionRegistrationsTable}.phone_number, {$optionRegistrationsTable}.language, {$optionRegistrationsTable}.status as winner_status
        FROM {$optionTable}
        INNER JOIN {$optionUsageTable} ON {$optionUsageTable}.optionId = {$optionTable}.id
        INNER JOIN {$optionRegistrationsTable} ON {$optionRegistrationsTable}.id = {$optionUsageTable}.userId
        WHERE {$optionTable}.status = 1
        ORDER BY {$optionTable}.id ASC, {$optionUsageTable}.createdAt ASC";
	$response = $wpdb->get_results( $sql, OBJECT );
 
    return rest_ensure_response( $response );
}

function getUserByEmail( $email ) {
    include( 'database-variables.php' );

    $sql = "SELECT {$optionRegistrationsTable}.*, ( SELECT COUNT( {$optionUsageTable}.id ) FROM {$optionUsageTable} WHERE {$optionUsageTable}.userId = {$optionRegistrationsTable}.id ) as 'usedSpins'
    FROM {$optionRegistrationsTable}
    WHERE email LIKE '{$email}' AND status = 1
    LIMIT 1";
    $response = $wpdb->get_results( $sql, OBJECT );

    if ( count( $response ) ) {
        return $response[0];
    }
    return null;
}

function buildPrizeCSV() {
    include( 'database-variables.php' );

    $sql = "SELECT {$optionUsageTable}.createdAt as wonAt, {$optionTable}.*, {$optionRegistrationsTable}.name, {$optionRegistrationsTable}.email, {$optionRegistrationsTable}.phone_number, {$optionRegistrationsTable}.language
        FROM {$optionUsageTable}
        LEFT JOIN {$optionTable} ON {$optionTable}.id = {$optionUsageTable}.optionId
        LEFT JOIN {$optionRegistrationsTable} ON {$optionRegistrationsTable}.id = {$optionUsageTable}.userId
        ORDER BY {$optionUsageTable}.createdAt ASC";

    $prizes = $wpdb->get_results( $sql, OBJECT );

    $csv = 'won_at,fillStyle,text,name,email,phone_number,language'."\n";
    foreach( $prizes as $prize ) {
        $data = array(
            $prize->wonAt,
            $prize->fillStyle,
            $prize->text,
            $prize->name,
            $prize->email,
            $prize->phone_number,
            $prize->language,
        );
        $csv .= implode( ',', $data )."\n";
    }

    $file_name = plugin_dir_path(__FILE__).'winners.csv';
    file_put_contents( $file_name, $csv );

    $response = array(
        'file' => plugin_dir_url( __FILE__ ).'winners.csv',
    );

    return rest_ensure_response( $response );
}

function getWins() {
    include( 'database-variables.php' );
    $response = array(
        'winners' => array(),
        'prizes' => array(),
    );

    $sql = "SELECT {$optionRegistrationsTable}.*
        FROM {$optionRegistrationsTable}
        WHERE status = 1";
    $response['winners'] = $wpdb->get_results( $sql, OBJECT );
    foreach( $response['winners'] as $user ) {
        $winSql = "SELECT {$optionUsageTable}.createdAt as wonAt, {$optionTable}.*
            FROM {$optionUsageTable} LEFT JOIN {$optionTable} ON {$optionTable}.id = {$optionUsageTable}.optionId
            WHERE {$optionUsageTable}.userId = {$user->id}";
        $user->wins = $wpdb->get_results( $winSql, OBJECT );
    }


    $sql = "SELECT {$optionUsageTable}.createdAt as wonAt, {$optionTable}.*
        FROM {$optionUsageTable} LEFT JOIN {$optionTable} ON {$optionTable}.id = {$optionUsageTable}.optionId
        WHERE {$optionUsageTable}.userId IS NULL OR {$optionUsageTable}.userId = 0";
    $response['prizes'] = $wpdb->get_results( $sql, OBJECT );

    return rest_ensure_response( $response );
}

function registerUser( WP_REST_Request $request ) {
    include( 'database-variables.php' );
    $userName = $request->get_param( 'name' );
    $userEmail = $request->get_param( 'email' );
    $phoneNumber = $request->get_param( 'phone_number' );
    $language = $request->get_param( 'language' );

    $registeredUser = getUserByEmail( $userEmail );
    
    if ( !$registeredUser ) {
        $wpdb->insert( 
            $optionRegistrationsTable, 
            array( 
                'name' => $userName,
                'email' => $userEmail,
                'phone_number' => $phoneNumber,
                'language' => $language,
                'createdAt' => current_time( 'mysql' ),
            )
        );
        
        $registeredUser = getUserByEmail( $userEmail );
    }
    return rest_ensure_response( $registeredUser );
}

function registerSpin( WP_REST_Request $request ) {
    include( 'database-variables.php' );
    $userEmail = $request->get_param( 'email' );
    $optionId = $request->get_param( 'optionId' );

    if ( $userEmail ) {
        $registeredUser = getUserByEmail( $userEmail );
    
        if ( $registeredUser ) {
            $wpdb->insert( 
                $optionUsageTable, 
                array( 
                    'userId' => $registeredUser->id,
                    'optionId' => $optionId,
                    'createdAt' => current_time( 'mysql' ),
                )
            );
    
            $registeredUser->usedSpins++;
        }
    } else {
        $wpdb->insert( 
            $optionUsageTable, 
            array( 
                'userId' => null,
                'optionId' => $optionId,
                'createdAt' => current_time( 'mysql' ),
            )
        );
    }

    return rest_ensure_response( $registeredUser );
}

function setOrbitSettings( WP_REST_Request $request ) {

    $usersCanRegister = $request->get_param( 'users_can_register' );
    update_option( 'orbitwebWinWheel-users_can_register', $usersCanRegister, true );
    
    $maxSpins = $request->get_param( 'max_spins' );
    update_option( 'orbitwebWinWheel-max_spins', $maxSpins, true );
    
    $layout = $request->get_param( 'layout' );
    update_option( 'orbitwebWinWheel-layout', $layout, true );
    
    $usersPhoneField = $request->get_param( 'users_phone_field' );
    update_option( 'orbitwebWinWheel-users_phone_field', $usersPhoneField, true );

    $dataCollection = $request->get_param( 'data_collection' );
    update_option( 'orbitwebWinWheel-data_collection', $dataCollection, true );

    return rest_ensure_response( true );
}

function setOptions( WP_REST_Request $request ) {
    include( 'database-variables.php' );
    
    $options = $request->get_param( 'options' );
    foreach( $options as $option ) {
        if ( $option['id'] ) {
            $wpdb->update( 
                $optionTable, 
                array( 
                    'fillStyle' => $option['fillStyle'],
                    'text' => $option['text'],
                    'textFontSize' => $option['textFontSize'],
                    'textFillStyle' => $option['textFillStyle'],
                    'strokeStyle' => $option['fillStyle'],
                    'percent' => ( $option['percent'] )? $option['percent'] : null,
                    'maxUses' => ( $option['maxUses'] )? $option['maxUses'] : null,
                    'status' => $option['status'],
                ), 
                array(
                    'id' => $option['id'],
                )
                );
            if ( $wpdb->last_error ) {
                return rest_ensure_response( $wpdb->last_error );
            }
        } else {
            $wpdb->insert( 
                $optionTable, 
                array( 
                    'fillStyle' => $option['fillStyle'],
                    'text' => $option['text'],
                    'textFontSize' => $option['textFontSize'],
                    'textFillStyle' => $option['textFillStyle'],
                    'strokeStyle' => $option['fillStyle'],
                    'percent' => $option['percent'],
                    'maxUses' => $option['maxUses'],
                    'status' => $option['status'],
                    'createdAt' => current_time( 'mysql' ),
                )
            );
        }
    }

    return getAllOptionsApi();
}
 
/**
 * This function is where we register our routes for our example endpoint.
 */
function orbitwebWinWheelRegisterRoutes() {

    register_rest_route( 'orbit/v1', '/available', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => WP_REST_Server::READABLE,
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'getAvailableOptionsApi',
	) );
	
    register_rest_route( 'orbit/v1', '/set_options', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'         => 'POST',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'setOptions',
        'args' => array(
            'options' => array(
                'type' => 'array',
                'required' => true,
            )
        ),
		'permissions_callback' => 'authPermissionCheck',
    ) );

    register_rest_route( 'orbit/v1', '/options', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => WP_REST_Server::READABLE,
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'getAllOptionsApi',
		'permissions_callback' => 'authPermissionCheck',
    ) );
	
    register_rest_route( 'orbit/v1', '/winners', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => WP_REST_Server::READABLE,
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        // 'callback' => 'getAllOptionsWinnersApi',
        'callback' => 'getWins',
		'permissions_callback' => 'authPermissionCheck',
    ) );

    register_rest_route( 'orbit/v1', '/winners_download', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => WP_REST_Server::READABLE,
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        // 'callback' => 'getAllOptionsWinnersApi',
        'callback' => 'buildPrizeCSV',
		'permissions_callback' => 'authPermissionCheck',
    ) );
	
    register_rest_route( 'orbit/v1', '/settings', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'  => 'POST',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        // 'callback' => 'getAllOptionsWinnersApi',
        'callback' => 'setOrbitSettings',
        'args' => array(
            'max_spins' => array(
                'type' => 'integer',
            ),
            'data_collection' => array(
                'type' => 'boolean',
            ),
            'users_can_register' => array(
                'type' => 'boolean',
            ),
            'layout' => array(
                'type' => 'string',
            ),
            'users_phone_field' => array(
                'type' => 'boolean',
            ),
        ),
		'permissions_callback' => 'authPermissionCheck',
    ) );
	
    register_rest_route( 'orbit/v1', '/register_user', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'         => 'POST',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'registerUser',
        'args' => array(
            'name' => array(
                'type' => 'string',
                'required' => true,
            ),
            'email' => array(
                'type' => 'string',
                'required' => true,
            ),
            'phone_number' => array(
                'type' => 'string',
                'required' => false,
            ),
            'language' => array(
                'type' => 'string',
                'required' => false,
            ),
        )
    ) );
	
    register_rest_route( 'orbit/v1', '/register_spin', array(
        // By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
        'methods'         => 'POST',
        // Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
        'callback' => 'registerSpin',
        'args' => array(
            'email' => array(
                'type' => 'string',
                'required' => false,
            ),
            'optionId' => array(
                'type' => 'integer',
                'required' => true,
            )
        )
    ) );
    
}
add_action( 'rest_api_init', 'orbitwebWinWheelRegisterRoutes' );

function authPermissionCheck(){
	return current_user_can( 'manage_options' );
}